﻿// 1512459.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512459.h"
#include <windowsX.h>
#include <winuser.h>
#include <CommCtrl.h>
#include <string>
#include <shlobj.h>
#include <shlwapi.h>
#include <objbase.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#define MAX_LOADSTRING 100
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")
#define EXCEPTION_EXECUTE_HANDLER 1
using namespace std;
// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512459, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512459));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_1512459));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512459);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, L"File_explorer", WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);

	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_FILE_EXIT:
			DestroyWindow(hWnd);
			break;

		case ID_FILE_SELECTALL:
			g_ListView->SelectAll();
			break;


		case ID_VIEW_ICONS:
			g_ListView->ChangeViewOption(LVS_ICON);
			break;

		case ID_VIEW_SMALLICONS:
			g_ListView->ChangeViewOption(LVS_SMALLICON);
			break;

		case ID_VIEW_LIST1:
			g_ListView->ChangeViewOption(LVS_LIST);
			break;

		case ID_VIEW_DETAILS1:
			g_ListView->ChangeViewOption(LVS_REPORT);
			break;
		}
	}
	break;

	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.

INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
HWND listv;
HWND sbar;
BOOL OnCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct)
{
	InitCommonControls();
	TV_INSERTSTRUCT tv;
	tv.hParent;
	RECT main;

	GetWindowRect(hwnd, &main);
	LPSHELLFOLDER psfDesktop = NULL;
	SHGetDesktopFolder(&psfDesktop);
	LPENUMIDLIST penumIDL = NULL;

	psfDesktop->EnumObjects(NULL, SHCONTF_FOLDERS, &penumIDL);
	LPITEMIDLIST pidl = NULL;
	HRESULT hr = NULL;
	WCHAR info[1024];
	info[0] = '\0';
	do {
		hr = penumIDL->Next(1, &pidl, NULL);
		if (hr == S_OK)
		{
			WCHAR buffer[1024];
			STRRET strret;
			hr = psfDesktop->GetDisplayNameOf(pidl, SHGDN_NORMAL, &strret);
			StrRetToBuf(&strret, pidl, buffer, 1024);
			StrCat(info, buffer);
			StrCat(info, L"\n");
			/*MALLOC_FREE_STRUCT(pidl);*/
		}
	} while (hr == S_OK);
	StrCat(info, '\0');
	psfDesktop->Release();
	CoTaskMemFree(pidl);
	/*MessageBox(0, info, 0, NULL);*/

	g_TreeView = new treeView;
	g_TreeView->Create(hwnd, IDC_TREEVIEW, g_hInst, main.right / 3, main.bottom);
	g_TreeView->LoadMyComputer(info);


	g_ListView = new listView;


	//Đã khởi tạo xong ứng dụng
	g_bStarted = TRUE;


	// cach tao list view thong thuong 
	listv = CreateWindowEx(0, WC_LISTVIEWW, L"",
		WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL | LVS_REPORT,
		380, 0, 650, 550, hwnd,
		NULL, hInst, NULL);
	SendMessage(listv, 0, NULL, true);
	LVCOLUMN lvCol;

	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_LEFT;

	lvCol.cx = 130;
	lvCol.pszText = _T("Name");
	ListView_InsertColumn(listv, 0, &lvCol);

	lvCol.fmt = LVCFMT_LEFT | LVCF_WIDTH;
	lvCol.cx = 100;
	lvCol.pszText = _T("Modify");
	ListView_InsertColumn(listv, 1, &lvCol);

	lvCol.fmt = LVCFMT_RIGHT;
	lvCol.pszText = _T("Size");
	ListView_InsertColumn(listv, 2, &lvCol);

	lvCol.pszText = _T("More");
	ListView_InsertColumn(listv, 3, &lvCol);

	LV_ITEM lv;
	WCHAR str[1024];
	int j = 0;
	for (int i = 0; info[i] != '\0'; ++i)
	{
		if (info[i] != '\0')
		{
			str[j] = info[i];
			j++;
		}
		if (info[i] == '\n')
		{

			str[j] = '\0';
			lv.iItem = 0;
			lv.mask = LVIF_TEXT;
			lv.iSubItem = 0;
			lv.pszText = str;
			lv.lParam = (LPARAM)(str);
			ListView_SetItem(listv, &lv);
			ListView_InsertItem(listv, &lv);
			WCHAR str[1024];
			j = 0;
		}
	}
	//tao status bar

	HLOCAL hloc;
	PINT paParts;
	int i, nWidth;
	InitCommonControls();
	sbar = CreateWindowEx(0, STATUSCLASSNAME, NULL,
		WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP,
		380, 0, 40, 20, hwnd, NULL, hInst, NULL);

	// gi nhan kich thuoc cua so chinh ( toa do va size)

	RECT rcClient;
	PWINDOWINFO pwi1 = 0;
	GetClientRect(hwnd, &rcClient);
	/*GetWindowInfo(hwnd,pwi1);*/
	fstream f;
	f.open("log.txt", ios::out);
	f << rcClient.bottom << endl;
	f << rcClient.left << endl;
	f << rcClient.right << endl;
	f << rcClient.top;

	f.close();
	// Allocate an array for holding the right edge coordinates.
	hloc = LocalAlloc(LHND, sizeof(int) * 3);
	paParts = (PINT)LocalLock(hloc);

	// Calculate the right edge coordinate for each part, and
	// copy the coordinates to the array.
	nWidth = rcClient.right / 3;
	int rightEdge = nWidth;
	for (i = 0; i < 3; i++) {
		paParts[i] = rightEdge;
		rightEdge += nWidth;
	}

	// Tell the status bar to create the window parts.
	SendMessage(sbar, SB_SETPARTS, (WPARAM)3, (LPARAM)paParts);
	SendMessage(sbar, SB_SETTEXT, 0, (LPARAM)L"status bar");

	// Free the array, and return.
	LocalUnlock(hloc);
	LocalFree(hloc);



	/*g_ListView->Create(hwnd, IDC_LISTVIEW, g_hInst, (main.right - main.left) * 2 / 3 + 1, main.bottom, main.right / 3);
	g_ListView->LoadMyComputer(info);*/
	/*LVITEM lvi;
	lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
	lvi.pszText = _T("adu");
	ListView_InsertItem(listv, &lvi);
	return true;*/


	//////

	return true;

}
