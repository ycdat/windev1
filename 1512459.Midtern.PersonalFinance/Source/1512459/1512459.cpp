﻿// 1512459.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512459.h"
#include "Lib.h"
#include <windowsx.h>
#include <commctrl.h>
#include <ObjIdl.h>
#include <gdiplus.h>
#pragma comment(lib, "gdiplus.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")
using namespace Gdiplus;
//using namespace std;
#define MAX_LOADSTRING 100
//filename to save.
#define FILE_PATH		L"testfile.txt"
// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND g_hWnd;
HWND hTemp;
HWND hClose;
HWND g_hListview;
GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR           gdiplusToken;
HWND hComboBox, hMoney, hContent, hSumMoney, hAdd;

int ItemCount = 0;
long SumMoney = 0;
//cac loai chi tieu.
WCHAR types[6][35] = { L"Ăn uống", L"Di chuyển", L"Nhà cửa", L"Xe cộ", L"Nhu yếu phẩm", L"Dịch vụ", };
unsigned long Ratio[6];
typedef struct Item
{
	WCHAR Type[40];
	unsigned long long Money;
	std::wstring Content;
};
std::vector<Item> listItem;
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
// fuction to write to file.
void writeToFile(std::wstring path);
//function to load from file.
void loadFromFile(std::wstring path);
// load to list view.
void loadToListView(HWND m_hListview);
//function to set window text
void setWindowText(HWND hWnd, long long value, std::wstring textBefore, std::wstring textAfter);

ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);



int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512459, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512459));

    MSG msg;
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
    return (int) msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_1512459));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512459);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
    return RegisterClassExW(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable
	HWND hWnd = CreateWindowW(szWindowClass, L"Quản lý chi tiêu", WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, 600, 700, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	return TRUE;
}
//setWindow Text
void setWindowText(HWND hWnd, long long value, std::wstring textBefore, std::wstring textAfter)
{
	WCHAR buffer[255];
	if (value < 10)
		wsprintf(buffer, L"0%I64d", value);
	else
		wsprintf(buffer, L"%I64d", value);
	SetWindowText(hWnd, (textBefore + std::wstring(buffer) + textAfter).c_str());
}
//write to file
void writeToFile(std::wstring path)
{
	const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
	std::wofstream file(path);
	file.imbue(utf8_locale);
	file << SumMoney << std::endl;
	for (int i = 0; i < listItem.size(); i++)
	{
		file << std::wstring(listItem[i].Type) << std::endl;
		file << std::wstring(listItem[i].Content) << std::endl;
		file << listItem[i].Money << std::endl;
	}
	file.close();
}
//load from file.
void loadFromFile(std::wstring path)
{
	const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
	std::wfstream file;
	file.imbue(utf8_locale);
	file.open(path, std::ios::in);
	std::wstring buffer;
	if (getline(file, buffer))
		SumMoney = _wtoi64(buffer.c_str());
	while (getline(file, buffer))
	{
		Item item;
		wcscpy_s(item.Type, buffer.c_str());
		getline(file, buffer);
		item.Content = buffer;
		getline(file, buffer);
		item.Money = _wtoi64(buffer.c_str());
		listItem.push_back(item);
	}
	file.close();
}
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
	{
		GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		INITCOMMONCONTROLSEX icc;
		icc.dwSize = sizeof(icc);
		icc.dwICC = ICC_WIN95_CLASSES;
		InitCommonControlsEx(&icc);
		LOGFONT lf;
		GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
		HFONT hFont = CreateFont(lf.lfHeight, lf.lfWidth,lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
			lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
			lf.lfPitchAndFamily, lf.lfFaceName);
		hComboBox = CreateWindowEx(0, WC_COMBOBOX, TEXT(""),CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE,
			40, 60, 110, 100, hWnd, NULL, hInst,NULL);
		SendMessage(hComboBox, WM_SETFONT, WPARAM(hFont), TRUE);
		
		hTemp = CreateWindowEx(0, L"STATIC", L"Số tiền:", WS_CHILD | WS_VISIBLE,300, 40, 40, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);
		HWND hGroupboxA = CreateWindowEx(0, L"BUTTON", L"Thêm một loại chi tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX | WS_GROUP,
			20, 10, 540, 120, hWnd, (HMENU)IDC_GROUPBOX, hInst, NULL);
		SendMessage(hGroupboxA, WM_SETFONT, WPARAM(hFont), TRUE);
		hAdd = CreateWindowEx(0, L"BUTTON", L"Thêm", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,	440, 57, 70, 25, hWnd, (HMENU)IDC_ADD, hInst, NULL);
		SendMessage(hAdd, WM_SETFONT, WPARAM(hFont), TRUE);
		hContent = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_MULTILINE | ES_AUTOVSCROLL, 170, 60, 110, 22, hWnd, NULL, hInst, NULL);
		SendMessage(hContent, WM_SETFONT, WPARAM(hFont), TRUE);
		hMoney = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_NUMBER, 300, 60, 110, 22, hWnd, NULL, hInst, NULL);
		SendMessage(hMoney, WM_SETFONT, WPARAM(hFont), TRUE);
		hTemp = CreateWindowEx(0, L"STATIC", L"Nội dung:", WS_CHILD | WS_VISIBLE, 170, 40, 50, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);
		hTemp = CreateWindowEx(0, L"STATIC", L"Loại chi tiêu:", WS_CHILD | WS_VISIBLE,40, 40, 65, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);
		
		TCHAR A[16];
		int  k;
		k=0;
		memset(&A, 0, sizeof(A));
		for (k = 0; k < 6; k++)
		{
			wcscpy_s(A, sizeof(A) / sizeof(TCHAR), (TCHAR*)types[k]);
			Ratio[k] = 0;
			{
				SendMessage(hComboBox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)A);

			}
		}
		SendMessage(hComboBox, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		HWND hGroupboxB = CreateWindowEx(0, L"BUTTON", L"Toàn bộ danh sách các chi tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX | WS_GROUP,
			20, 150, 540, 200, hWnd, (HMENU)IDC_GROUPBOX, hInst, NULL);
		SendMessage(hGroupboxB, WM_SETFONT, WPARAM(hFont), TRUE);
		long extStyle = WS_EX_CLIENTEDGE;
		long style = LVS_REPORT | LVS_ICON | LVS_EDITLABELS | LVS_SHOWSELALWAYS;
		g_hListview = CreateWindowEx(extStyle, WC_LISTVIEW, _T("List View"),
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | style,
			40, 180, 500, 150, hWnd, (HMENU)IDL_LISTVIEW, hInst, NULL);
		LVCOLUMN lvCol;
		lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
		lvCol.fmt = LVCFMT_LEFT;
		lvCol.cx = 165;
		lvCol.pszText = _T("Loại chi tiêu");
		ListView_InsertColumn(g_hListview, 0, &lvCol);
		lvCol.fmt = LVCFMT_LEFT;
		lvCol.cx = 165;
		lvCol.pszText = _T("Nội dung");
		ListView_InsertColumn(g_hListview, 1, &lvCol);
		lvCol.fmt = LVCFMT_LEFT;
		lvCol.pszText = _T("Số tiền");
		lvCol.cx = 120;
		ListView_InsertColumn(g_hListview, 2, &lvCol);
		HWND hGroupboxC = CreateWindowEx(0, L"BUTTON", L"Thông tin thống kê", WS_CHILD | WS_VISIBLE | BS_GROUPBOX | WS_GROUP,20, 370, 540, 205, hWnd, (HMENU)IDC_GROUPBOX, hInst, NULL);
		SendMessage(hGroupboxC, WM_SETFONT, WPARAM(hFont), TRUE);
		hSumMoney = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_READONLY,250, 400, 110, 22, hWnd, NULL, hInst, NULL);
		SendMessage(hSumMoney, WM_SETFONT, WPARAM(hFont), TRUE);
		hTemp = CreateWindowEx(0, L"STATIC", L"Tổng cộng:", WS_CHILD | WS_VISIBLE, 175, 403, 55, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);
		hTemp = CreateWindowEx(0, L"STATIC", L"Ăn uống", WS_CHILD | WS_VISIBLE, 60, 537, 55, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);
		hTemp = CreateWindowEx(0, L"STATIC", L"Nhu yếu phẩm", WS_CHILD | WS_VISIBLE, 370, 537, 90, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);
		hTemp = CreateWindowEx(0, L"STATIC", L"Dịch vụ", WS_CHILD | WS_VISIBLE, 460, 537, 55, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);
		hTemp = CreateWindowEx(0, L"STATIC", L"Di chuyển", WS_CHILD | WS_VISIBLE, 140, 537, 55, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);
		hTemp = CreateWindowEx(0, L"STATIC", L"Nhà cửa", WS_CHILD | WS_VISIBLE, 220, 537, 55, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);
		hTemp = CreateWindowEx(0, L"STATIC", L"Xe cộ", WS_CHILD | WS_VISIBLE, 300, 537, 55, 15, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);
		hAdd = CreateWindowEx(0, L"BUTTON", L"Thoát", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,240, 590, 90, 35, hWnd, (HMENU)IDC_CLOSE, hInst, NULL);
		SendMessage(hAdd, WM_SETFONT, WPARAM(hFont), TRUE);
		loadFromFile(FILE_PATH);
		loadToListView(g_hListview);
		ItemCount = listItem.size();
		setWindowText(hSumMoney, SumMoney, L"", L"");
		break;
	}
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		switch (wmId)
		{
		case IDC_ADD:
		{
			Item item;
			WCHAR* buffer;
			int len = GetWindowTextLength(hContent);
			if (len > 0)
			{
				buffer = new WCHAR[len + 1];
				GetWindowText(hContent, buffer, len + 1);
				item.Content = std::wstring(buffer);
			}
			else
			{
				MessageBox(g_hWnd, L"Bạn chưa điền Nội dung!!", L"Error", MB_OK);
				break;
			}
			len = GetWindowTextLength(hMoney);
			if (len > 0)
			{
				buffer = new WCHAR[len + 1];
				GetWindowText(hMoney, buffer, len + 1);
				item.Money = _wtoi64(buffer);
			}
			else
			{
				MessageBox(g_hWnd, L"Bạn chưa điền vào Số tiền!!", L"Error", MB_OK);
				break;
			}
			buffer = new WCHAR[20];
			GetWindowText(hComboBox, buffer, 20);
			wcscpy_s(item.Type, buffer);
			listItem.push_back(item);
			if (wcscmp(item.Type, L"Ăn uống") == 0)
				Ratio[0] += item.Money;
			else if (wcscmp(item.Type, L"Di chuyển") == 0)
				Ratio[1] += item.Money;
			else if (wcscmp(item.Type, L"Nhà cửa") == 0)
				Ratio[2] += item.Money;
			else if (wcscmp(item.Type, L"Xe cộ") == 0)
				Ratio[3] += item.Money;
			else if (wcscmp(item.Type, L"Nhu yếu phẩm") == 0)
				Ratio[4] += item.Money;
			else if (wcscmp(item.Type, L"Dịch vụ") == 0)
				Ratio[5] += item.Money;
			SumMoney += item.Money;
			LV_ITEM lv;
			lv.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;
			lv.iItem = ItemCount;
			lv.iSubItem = 0;
			lv.pszText = item.Type;
			ListView_InsertItem(g_hListview, &lv);
			lv.mask = LVIF_TEXT;
			lv.iSubItem = 1;
			lv.pszText = (WCHAR*)item.Content.c_str();
			ListView_SetItem(g_hListview, &lv);
			lv.iSubItem = 2;
			buffer = new WCHAR[20];
			wsprintf(buffer, L"%I64d", item.Money);
			lv.pszText = buffer;
			ListView_SetItem(g_hListview, &lv);
			setWindowText(hSumMoney, SumMoney, L"", L"");
			RedrawWindow(g_hWnd, NULL, NULL, RDW_INTERNALPAINT | RDW_UPDATENOW | RDW_ERASENOW | RDW_INVALIDATE);
			RedrawWindow(hWnd, NULL, NULL, RDW_INTERNALPAINT | RDW_UPDATENOW | RDW_ERASENOW | RDW_INVALIDATE);
			break;
		}
		case IDC_CLOSE:
			writeToFile(FILE_PATH);
			DestroyWindow(hWnd);
			break;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			writeToFile(FILE_PATH);
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		Pen* pen = new Pen(Color(255, 0, 0, 0), 1);
		Graphics* graphics = new Graphics(hdc);
		HatchBrush* Blue = new HatchBrush(HatchStyleCross, Color(255, 0, 0, 160), Color(255, 0, 0, 160));
		HatchBrush* Purple = new HatchBrush(HatchStyleCross, Color(255, 128, 0, 128), Color(255, 128, 0, 128));
		HatchBrush* Orange = new HatchBrush(HatchStyleCross, Color(255, 255, 100, 0), Color(255, 255, 100, 0));
		HatchBrush* Yellow = new HatchBrush(HatchStyleCross, Color(255, 255, 255, 0), Color(255, 255, 255, 0));
		HatchBrush* Red = new HatchBrush(HatchStyleCross, Color(255, 255, 0, 0), Color(255, 255, 0, 0));
		HatchBrush* Green = new HatchBrush(HatchStyleCross, Color(255, 0, 128, 0), Color(255, 0, 128, 0));
		int pos = 40;
		graphics->DrawRectangle(pen, pos, 443, 497, 35);
		if (SumMoney != 0)
			for (int i = 0; i < 6; i++)
			{
				graphics->DrawRectangle(pen, pos, 443, 500 * Ratio[i] / SumMoney, 35);
				if (i == 0)
				{
					graphics->FillRectangle(Red, pos, 443, 500 * Ratio[i] / SumMoney, 35);
				}
				else if (i == 1)
				{
					graphics->FillRectangle(Green, pos, 443, 500 * Ratio[i] / SumMoney, 35);
				}
				else if (i == 2)
				{
					graphics->FillRectangle(Blue, pos, 443, 500 * Ratio[i] / SumMoney, 35);
				}
				else if (i == 3)
				{
					graphics->FillRectangle(Yellow, pos, 443, 500 * Ratio[i] / SumMoney, 35);
				}
				else if (i == 4)
				{
					graphics->FillRectangle(Orange, pos, 443, 500 * Ratio[i] / SumMoney, 35);
				}
				else
				{
					graphics->FillRectangle(Purple, pos, 443, 500 * Ratio[i] / SumMoney, 35);
				}
				pos += 500 * Ratio[i] / SumMoney;
			}
		graphics->DrawRectangle(pen, 40, 510, 100, 20);
		graphics->FillRectangle(Red, 40, 510,100, 20);
		graphics->DrawRectangle(pen, 120, 510, 100, 20);
		graphics->FillRectangle(Green, 120, 510, 100, 20);
		graphics->DrawRectangle(pen, 200, 510, 100, 20);
		graphics->FillRectangle(Blue, 200, 510, 100, 20);
		graphics->DrawRectangle(pen, 280, 510, 100, 20);
		graphics->FillRectangle(Yellow, 280, 510, 100, 20);
		graphics->DrawRectangle(pen, 360, 510, 100, 20);
		graphics->FillRectangle(Orange, 360, 510, 100, 20);
		graphics->DrawRectangle(pen, 440, 510, 100, 20);
		graphics->FillRectangle(Purple,440, 510, 100, 20);
		delete pen;
		delete graphics;
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		writeToFile(FILE_PATH);
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void loadToListView(HWND m_hListview)
{
	LV_ITEM lv;
	WCHAR* buffer = new WCHAR[20];
	for (int i = 0; i < listItem.size(); i++)
	{
		lv.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;
		lv.iItem = i;
		lv.iSubItem = 0;
		lv.pszText = listItem[i].Type;
		ListView_InsertItem(m_hListview, &lv);
		lv.mask = LVIF_TEXT;
		lv.iSubItem = 1;
		lv.pszText = (WCHAR*)listItem[i].Content.c_str();
		ListView_SetItem(m_hListview, &lv);
		lv.iSubItem = 2;
		buffer = new WCHAR[20];
		wsprintf(buffer, L"%I64d", listItem[i].Money);
		lv.pszText = buffer;
		ListView_SetItem(m_hListview, &lv);
		if (wcscmp(listItem[i].Type, L"Ăn uống") == 0)
			Ratio[0] += listItem[i].Money;
		else if (wcscmp(listItem[i].Type, L"Di chuyển") == 0)
			Ratio[1] += listItem[i].Money;
		else if (wcscmp(listItem[i].Type, L"Nhà cửa") == 0)
			Ratio[2] += listItem[i].Money;
		else  if (wcscmp(listItem[i].Type, L"Xe cộ") == 0)
			Ratio[3] += listItem[i].Money;
		else if (wcscmp(listItem[i].Type, L"Nhu yếu phẩm") == 0)
			Ratio[4] += listItem[i].Money;
		else
			Ratio[5] += listItem[i].Money;
	}
}