===================================================================================================   
     	 Họ tên: Đoàn Hoài Sơn
         Mssv: 1512459
================================================================================================
         Danh sách các chức năng đã làm được:
	   -Thêm vào danh sách một mục chi, gồm 3 thành phần.
	   -Xem lại danh sách các mục chi của mình, lưu và nạp vào tập tin text.
	   -Khi chương trình chạy lên tự động nạp danh sách chi tiêu từ tập tin text lên và hiển thị.
	   -Khi chương trình thoát thì tự động lưu danh sách mới vào tập tin text.
	   -Vẽ biểu đồ  nhằm biểu diễn trực quan tỉ lệ tiêu xài.
=================================================================================================
	Hướng dẫn sử dụng:
	- Người dùng chọn loại chi tiêu.
	- Sau đó người dùng thêm số tiền,và nội dung (2 nội dung này không được bỏ trống) và nhấn nút "Thêm".
	- Ở bên dưới sẽ hiển thị tổng tiền và biểu đồ trực quan thể hiện tỉ lệ tiêu xài.
	- Khi nhấn nút "Thoát" thì chương trình sẽ lưu thông tin vào tập tin "testfile.txt" và Thoát.
===================================================================================================
Link repository:https://bitbucket.org/ycdat/windev1/src/accd107b23f9e79db07bd3ddf579c143029cb3fb/1512459.Midtern.PersonalFinance/?at=master
Link Youtube: https://youtu.be/ul2Ynj2-nxQ
===================================================================================================

